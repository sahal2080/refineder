# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'refineder/version'

Gem::Specification.new do |spec|

  spec.name        = 'refineder'
  spec.version       = Refineder::VERSION
  spec.author      = 'Sahal Alarabi'
  spec.email       = 'sahal@menuslate.com'
  spec.summary     = 'A collection of refinements for Ruby core classes.'
  spec.description = <<-EOS
Refineder is a refinement-creator for extending the core
capabilities of Ruby's built-in classes.
  EOS
  spec.homepage = 'https://gitlab.com/sahal2080/refineder'
  spec.license  = 'JSON'

  spec.platform              = Gem::Platform::RUBY
  spec.required_ruby_version = '>= 2'
  spec.has_rdoc              = 'yard'

  spec.files         = Dir["lib/**/*"]
  spec.require_paths = ['lib']


  spec.add_dependency 'finder'
  spec.add_dependency 'corefines'


  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'facets'
  spec.add_development_dependency 'activesupport'
end
